import RxSwift
import RxCocoa

protocol API {
    func getCarsList() -> Observable<[Car]>
}

class DefaultAPI: API {
    let URLSession: URLSession
    let backgroundWorkScheduler: ImmediateSchedulerType
    let mainScheduler: SerialDispatchQueueScheduler
    let reachabilityService: ReachabilityService
    
    let loadingData = ActivityIndicator()
    
    init(URLSession: URLSession, backgroundWorkScheduler: ImmediateSchedulerType, mainScheduler: SerialDispatchQueueScheduler, reachabilityService: ReachabilityService) {
        self.URLSession = URLSession
        self.backgroundWorkScheduler = backgroundWorkScheduler
        self.mainScheduler = mainScheduler
        self.reachabilityService = reachabilityService
    }
    
    func getCarsList() -> Observable<[Car]> {
        guard let url = URL(string: "https://wunder-test-case.s3-eu-west-1.amazonaws.com/ios/locations.json") else {
            return Observable.error(apiError("Can't create url"))
        }
        return getJson(from: url)
    }
    
    private func getJson(from url: URL) -> Observable<[Car]> {
        return URLSession.rx.json(url: url)
            .trackActivity(loadingData)
            .retry(3)
            .retryOnBecomesReachable([], reachabilityService: self.reachabilityService)
            .startWith([])
            .map(parse)
    }
    
    private func parse(json: Any) -> [Car] {
        do {
            let data = try JSONSerialization.data(withJSONObject: json, options: [])
                let objs = try JSONDecoder().decode(Placemarks.self, from: data)
                let cars = objs.placemarks
                return cars
        } catch {
            return []
        }
    }
}
