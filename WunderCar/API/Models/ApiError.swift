import Foundation

enum ApiError: Error {
    case requestFailed(statusCode: Int?, response: Data?)
}

func apiError(_ error: String) -> NSError {
    return NSError(domain: "API", code: -1, userInfo: [NSLocalizedDescriptionKey: error])
}
