import RxSwift
import CoreLocation
import class Foundation.NSDictionary

enum Condition: String, Codable {
    case good = "GOOD"
    case unacceptable = "UNACCEPTABLE"
}

enum EngineType: String, Codable, RawRepresentable {
    case ce = "CE"
}

struct Car: Codable {
    let address: String
    let coordinates: [Double]
    let engineType: EngineType
    let exterior: Condition
    let fuel: Int
    let interior: Condition
    let name: String
    let vin: String
}

struct Placemarks: Codable {
    let placemarks: [Car]
}
