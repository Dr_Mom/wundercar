import Foundation
import RxSwift

let carsKey = "kCars"

protocol DBProtocol {
    func store(_ object: [Car], for key: String)
    func loadObject(for key: String) -> Observable<[Car]>
}

class DBService: DBProtocol {
    
    func store(_ object: [Car], for key: String) {
        let encoded = try? JSONEncoder().encode(object)
        UserDefaults.standard.set(encoded, forKey: key)
    }
    
    func loadObject(for key: String) -> Observable<[Car]> {
        guard let data = UserDefaults.standard.object(forKey: key) as? Data else { return Observable.empty() }
        guard let savedData = try? JSONDecoder().decode([Car].self, from: data) else { return Observable.empty() }
        
        return Observable.just(savedData)
    }
}
