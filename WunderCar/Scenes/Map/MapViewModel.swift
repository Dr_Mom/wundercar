import RxSwift

class MapViewModel {
    private let API: API
    private let DB: DBProtocol
    let selectedCar: Car?
    
    private let disposeBag = DisposeBag()
    
    init(API: API, DB: DBProtocol, selectedCar: Car?) {
        self.API = API
        self.DB = DB
        self.selectedCar = selectedCar
    }
    
    lazy var cars: Observable<[Car]> = {
        return DB.loadObject(for: carsKey)
    }()
}
