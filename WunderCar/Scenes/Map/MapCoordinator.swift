import RxSwift

class MapCoordinator: BaseCoordinator<Void> {

    private let navigationController: UINavigationController?
    private let car: Car?
    
    init(navigationController: UINavigationController?, car: Car?) {
        self.navigationController = navigationController
        self.car = car
    }
    
    override func start() -> Observable<CoordinationResult> {
        let viewModel = MapViewModel(API: dependencies.API, DB: dependencies.DB, selectedCar: car)
        let viewController = MapViewController()
        
        viewController.viewModel = viewModel
        viewController.title = "Map"
        
        navigationController?.pushViewController(viewController, animated: true)
        
        return Observable.never()
    }
}

