import RxSwift
import MapKit
import RxMKMapView

class MapViewController: UIViewController {
    
    private let disposeBag = DisposeBag()
    var viewModel: MapViewModel!

    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpBindings()
    }
    
    private func setUpBindings() {
        
        viewModel.cars
            .map { self.loadPointsOfInterest($0) }
            .withLatestFrom(mapView.rx.region) { ($0, $1) }
            .map { points, region -> [MKAnnotation] in
                return points
        }
        .do(afterCompleted: {
            guard let car = self.viewModel?.selectedCar else {
                let haburgCoord = CLLocationCoordinate2D(latitude: Coordinates.hamburgLat, longitude: Coordinates.hamubrgLong)
                let viewRegion = MKCoordinateRegion(center: haburgCoord, latitudinalMeters: 10000, longitudinalMeters: 10000)
                self.mapView.setRegion(viewRegion, animated: false)
                return
            }
            let name = car.name
            let coordinates = car.coordinates
            let lat = coordinates[1]
            let lon = coordinates[0]
            let coord = CLLocationCoordinate2D(latitude: lat, longitude: lon)
            let poi = PointOfInterest(title: name, coordinate: coord)
            DispatchQueue.main.async {
                self.mapView.showAnnotations([poi], animated: true)
                self.mapView.selectAnnotation(poi, animated: true)
            }
        })
            .asDriver(onErrorJustReturn: [])
            .drive(mapView.rx.annotations)
            .disposed(by: disposeBag)
        
        mapView.rx
            .setDelegate(self)
            .disposed(by: disposeBag)
    }
    
    private func loadPointsOfInterest(_ cars: [Car]) -> [PointOfInterest] {
        print("Loading POIs...")
        let cars = cars.compactMap { car -> PointOfInterest? in
            let name = car.name
            let coordinates = car.coordinates
            let lat = coordinates[1]
            let lon = coordinates[0]
            let coord = CLLocationCoordinate2D(latitude: lat, longitude: lon)
            return PointOfInterest(title: name, coordinate: coord)
        }
        print("Found \(cars.count) POIs")
        return cars
    }
}

// MARK: - MKMapView Delegates
extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "annotation")
        view.tintColor = .green
        view.canShowCallout = true
        return view
    }

    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        views.forEach { $0.alpha = 0.0 }

        UIView.animate(withDuration: 0.4,
                       animations: {
                        views.forEach { $0.alpha = 1.0 }
                       })
    }
}

// MARK: - Map Annotation and Helpers
class PointOfInterest: NSObject, MKAnnotation {
    let coordinate: CLLocationCoordinate2D
    let title: String?

    init(title: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.coordinate = coordinate
    }
}

extension MKCoordinateRegion {
    func contains(poi: PointOfInterest) -> Bool {
        return abs(self.center.latitude - poi.coordinate.latitude) <= self.span.latitudeDelta / 2.0
            && abs(self.center.longitude - poi.coordinate.longitude) <= self.span.longitudeDelta / 2.0
    }
}
