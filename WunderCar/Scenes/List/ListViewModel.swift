import RxSwift

class ListViewModel {
    let API: API
    let DB: DBProtocol
    private let disposeBag = DisposeBag()
    
    /// Inputs
    let tapCell: AnyObserver<Car?>
    let tapMap: AnyObserver<Void>

    /// Outputs
    let showCarOnMap: Observable<Car?>
    let showMapScreen: Observable<Void>

    init(API: API, DB: DBProtocol) {
        self.API = API
        self.DB = DB
        
        let _chooseCar = PublishSubject<Car?>()
        self.tapCell = _chooseCar.asObserver()
        self.showCarOnMap = _chooseCar.asObservable()
        
        let _tapButton = PublishSubject<Void>()
        self.tapMap = _tapButton.asObserver()
        self.showMapScreen = _tapButton.asObservable()
    }
    
    lazy var cars: Observable<[Car]> = {
        return API.getCarsList()
            .do(onNext: { cars in
                self.DB.store(cars, for: carsKey)
            })
    }()
}
