import RxSwift
import RxCocoa

public class CarCell: UITableViewCell, ReusableView {
    
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var engineTypeLabel: UILabel!
    @IBOutlet weak var exteriorLabel: UILabel!
    @IBOutlet weak var interiorLabel: UILabel!
    @IBOutlet weak var vinLabel: UILabel!
    @IBOutlet weak var fuelLabel: UILabel!
    
    var disposeBag: DisposeBag?
    
    var reachabilityService: ReachabilityService?
    
    public override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    var car: Car? {
        didSet {
            adressLabel.text = car?.address
            engineTypeLabel.text = car?.engineType.rawValue
            exteriorLabel.text = car?.exterior.rawValue
            interiorLabel.text = car?.interior.rawValue
            vinLabel.text = car?.vin
            fuelLabel.text = String(car?.fuel ?? 0)
        }
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        
        self.car = nil
        self.disposeBag = nil
    }
    
    deinit {
    }
}

private protocol ReusableView: class {
    var disposeBag: DisposeBag? { get }
    func prepareForReuse()
}
