import RxSwift

class ListCoordinator: BaseCoordinator<Void> {
    
    private let window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    override func start() -> Observable<Void> {
        
        let viewModel = ListViewModel(API: dependencies.API, DB: dependencies.DB)
        let viewController = ListViewController()

        viewController.viewModel = viewModel
        viewController.title = "Cars List"
        let navigationController = UINavigationController(rootViewController: viewController)

        viewModel.showMapScreen
            .subscribe(onNext: { [weak self] in self?.showMap(on: viewController.navigationController, car: nil) })
            .disposed(by: disposeBag)
        
        viewModel.showCarOnMap
            .subscribe(onNext: { [weak self] in self?.showMap(on: viewController.navigationController, car: $0) })
            .disposed(by: disposeBag)
                
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
        
        return Observable.never()
    }
    
    @discardableResult private func showMap(on navigationController: UINavigationController?, car: Car?) -> Observable<Void> {
        let coordinator = MapCoordinator(navigationController: navigationController, car: car)
        return coordinate(to: coordinator)
    }
}
