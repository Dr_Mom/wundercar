import RxSwift

class ListViewController: UIViewController {
    
    private let disposeBag = DisposeBag()
    var viewModel: ListViewModel!
    var reachabilityService: ReachabilityService!
    
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var mapButton: FancyButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpBindings()
        configureTableDataSource()
        configureNavigateOnRowClick()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let indexPath = listTableView.indexPathForSelectedRow {
            listTableView.deselectRow(at: indexPath, animated: false)
        }
    }
    
    private func setUpBindings() {
        mapButton.rx.tap
            .bind(to: viewModel.tapMap)
            .disposed(by: disposeBag)
    }
    
    private func configureTableDataSource() {
        listTableView.register(UINib(nibName: "CarCell", bundle: nil), forCellReuseIdentifier: "CarCell")
        
        listTableView.rowHeight = 160
        listTableView.hideEmptyCells()
        
        getCars()
    }
    
    private func getCars() {
        viewModel.cars
            .asDriver(onErrorJustReturn: [])
            .drive(listTableView.rx.items(cellIdentifier: "CarCell", cellType: CarCell.self)) { (_, element, cell) in
                cell.car = element
            }
            .disposed(by: disposeBag)
    }
    
    private func configureNavigateOnRowClick() {
        listTableView.rx.modelSelected(Car.self)
            .asDriver()
            .drive(viewModel.tapCell)
            .disposed(by: disposeBag)
    }
}
