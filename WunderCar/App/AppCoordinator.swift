import RxSwift

class AppCoordinator: BaseCoordinator<Void> {

    private let window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }

    override func start() -> Observable<Void> {
        let coordinator = ListCoordinator(window: window)
        return coordinate(to: coordinator)
    }
}
