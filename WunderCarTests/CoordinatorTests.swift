import XCTest
@testable import WunderCar

class CoordinatorTests: XCTestCase {
    
    func test_Map_ContainerViewContollerIsPushedInNavigationController() {
        let mockNavigationController = MockNavigationController()
        let dashboardContainerCoordinator = MapCoordinator(navigationController: mockNavigationController, car: nil)
        
        _ = dashboardContainerCoordinator.start()
        
        XCTAssertTrue(mockNavigationController.isPushCalled)
        XCTAssertNotNil(mockNavigationController.viewControllerPushed as? MapViewController)
    }
}

private class MockNavigationController: UINavigationController {
    
    var isPushCalled = false
    var viewControllerPushed: UIViewController?
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        isPushCalled = true
        viewControllerPushed = viewController
    }
}
