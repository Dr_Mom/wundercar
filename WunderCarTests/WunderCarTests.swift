import XCTest
import MapKit
import RxSwift
import RxCocoa
import RxMKMapView
@testable import WunderCar

class WunderCarTests: XCTestCase {
    var disposeBag: DisposeBag!

    override func setUp() {
        disposeBag = DisposeBag()
    }
    
    override func tearDown() {
        disposeBag = nil
        super.tearDown()
    }
    
    func test_rx_willStartLoadingMap() {
        let mapView = MKMapView()
        var called = false
        
        autoreleasepool {
            
            _ = mapView.rx.willStartLoadingMap
                .subscribe(onNext: {
                    called = true
                })
                .disposed(by: disposeBag)
            
            mapView.delegate!.mapViewWillStartLoadingMap!(mapView)
        }
        
        XCTAssertTrue(called)
    }
    
    func test_rx_didFinishLoadingMap() {
        let mapView = MKMapView()
        var called = false
        
        autoreleasepool {
            
            _ = mapView.rx.didFinishLoadingMap
                .subscribe(onNext: {
                    called = true
                })
                .disposed(by: disposeBag)

            mapView.delegate!.mapViewDidFinishLoadingMap!(mapView)
        }
        
        XCTAssertTrue(called)
    }
    
    func test_rx_didAddAnnotationViews() {
        let mapView = MKMapView()
        var resultViews:[MKAnnotationView]?
        
        let views = [MKAnnotationView()]
        autoreleasepool {
            _ = mapView.rx.didAddAnnotationViews
                .subscribe(onNext: {
                    resultViews = $0
                })
                .disposed(by: disposeBag)

            mapView.delegate!.mapView!(mapView, didAdd: views)
        }
        
        XCTAssertNotNil(resultViews)
        XCTAssertEqual(resultViews, views)
    }
    
    func test_rx_didSelectAnnotationView() {
        let mapView = MKMapView()
        var resultView: MKAnnotationView?
        
        let view = MKAnnotationView()
        
        autoreleasepool {
            _ = mapView.rx.didSelectAnnotationView
                .subscribe(onNext: {
                    resultView = $0
                })
                .disposed(by: disposeBag)

            mapView.delegate!.mapView!(mapView, didSelect: view)
        }
        
        XCTAssertNotNil(resultView)
        XCTAssertEqual(resultView, view)
    }
    
    func test_rx_didDeselectAnnotationView() {
        let mapView = MKMapView()
        var resultView: MKAnnotationView?
        
        let view = MKAnnotationView()
        
        autoreleasepool {
            _ = mapView.rx.didDeselectAnnotationView
                .subscribe(onNext: {
                    resultView = $0
                })
                .disposed(by: disposeBag)
            
            mapView.delegate!.mapView!(mapView, didDeselect: view)
        }
        
        XCTAssertNotNil(resultView)
        XCTAssertEqual(resultView, view)
    }
}
